package com.surveysensum.tests;

import static com.surveysensum.helperUtils.ConfigPropertyReader.getProperty;

import org.testng.annotations.Test;

import com.surveysensum.framework.PreAndPostTestEvents;
import com.surveysensum.keywords.HomePageFunctions;
import com.surveysensum.keywords.LoginPageFunctions;

public class SSPHomePageTest extends PreAndPostTestEvents {
	LoginPageFunctions loginPage;
	HomePageFunctions homePage;
	public void init() {
		 loginPage = new LoginPageFunctions(driver);
		 homePage = new HomePageFunctions(driver);
	}
	
	
	@Test(description = "1. Login to survey sensum portal")
	void TC01_LoginToPortal() {
		loginPage.loginToSurveySensumPortal(getProperty("UATURL"), getProperty("UATusername"), getProperty("UATpassword"));
		}
	
	@Test(description = "2. Invite users to platform")
	void TC02_InviteUsersToPlatform() {	
		homePage.inviteUsers();
	}
	
	@Test(description="3. Create Survey")
	void TC03_CreateSurvey() {
		homePage.createSurvey("Customer Effort Score Survey","Customer Effort Score Survey-MSV"); //Survey Type is the full name of survey type we want to create
	}
	
	@Test(description="4. Archive Survey")
	void TC04_ArchiveSurvey() {
		homePage.archiveExistingSurvey("Customer Effort Score Survey-MSV");
	}
	
	@Test(description="5. Delete Survey from Archives")
	void TC05_DeleteSurveyFromArchive() {
		homePage.navigateToArchives();
		homePage.deleteSurveyFromArchive("Customer Effort Score Survey-MSV");
	}
	
	@Test(description="6. People tab - Add Contact")
	void TC06_PeopleTabAddContactVerification()  {
		homePage.navigateToPeopleTab();
		homePage.addContactsInPeopleTab("Smoke Singh","smokeuser@neurosensum.com", "9999888800");
	}
	
	
	@Test(description="7. People tab - delete Contact")
	void TC07_PeopleTabDeleteContactVerification() {
		homePage.navigateToPeopleTab();
		homePage.searchUserInPeopleTab("smokeuser@neurosensum.com");
		homePage.delete1Contact("smokeuser@neurosensum.com");
	}
	
	@Test(description="8. Settings - Subscription")
	void TC08_SettingsTabVerifySubscriptionPlan() {
		homePage.navigateToSettingsTabs();
		homePage.verifyUserSubscriptionPlan("Enterprise","Monthly plan");
	}

	@Test(description = "10. Logout from survey sensum portal")
	void TC10_LogOutFromPortal() {
		loginPage.logoutFromPortal();
	}
}
