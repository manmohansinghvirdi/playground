package com.surveysensum.tests;

import static com.surveysensum.helperUtils.ConfigPropertyReader.getProperty;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.surveysensum.framework.PreAndPostTestEvents;
import com.surveysensum.keywords.HomePageFunctions;
import com.surveysensum.keywords.LoginPageFunctions;

public class HouseKeepingTest extends PreAndPostTestEvents{
	LoginPageFunctions loginPage;
	HomePageFunctions homePage;
	
	public void init() {
		 loginPage = new LoginPageFunctions(driver);
		 homePage = new HomePageFunctions(driver);
	}
	
	@Test(description="1. Login To Portal")
	void TC01_LoginToPortal() {
		loginPage.loginToSurveySensumPortal(getProperty("UATURL"), getProperty("UATusername"), getProperty("UATpassword"));
		}
	
	@Parameters({"SurveyName"})
	@Test(description="2. Archive Survey")
	void TC02_ArchiveSurvey(@Optional("Star Survey Smoke Test")String name) {
		homePage.archiveExistingSurvey(name);
	}
	
	@Parameters({"SurveyName"})
	@Test(description="3. Delete Survey from Archives")
	void TC03_DeleteSurveyFromArchive(@Optional("Star Survey Smoke Test")String name) {
		homePage.navigateToArchives();
		homePage.deleteSurveyFromArchive(name);
	}
	
	@Test(description = "4. Logout from survey sensum portal")
	void TC04_LogOutFromPortal() {
	
		loginPage.logoutFromPortal();
	}
}
