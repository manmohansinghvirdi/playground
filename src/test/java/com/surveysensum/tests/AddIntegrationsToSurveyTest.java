package com.surveysensum.tests;

import static com.surveysensum.helperUtils.ConfigPropertyReader.getProperty;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.surveysensum.framework.PreAndPostTestEvents;
import com.surveysensum.keywords.IntegrationsPageFunctions;
import com.surveysensum.keywords.LoginPageFunctions;

public class AddIntegrationsToSurveyTest extends PreAndPostTestEvents {
	
	IntegrationsPageFunctions integrtnPage;
	
	LoginPageFunctions loginPage;
	public void init() {
		 loginPage = new LoginPageFunctions(driver);
		 integrtnPage = new IntegrationsPageFunctions(driver);
	}
	
	@Test(description = "1. Login to survey sensum portal")
	void TC01_LoginToPortal() {
		loginPage.loginToSurveySensumPortal(getProperty("UATURL"), getProperty("UATusername"), getProperty("UATpassword"));
	}
	
	
	@Parameters({"SurveyName"})
	@Test(description="2. Launch Integration Page")
	void TC02_LaunchIntegrationPageOfSurvey(@Optional("Star Survey Smoke Test")String surveyName) {
		integrtnPage.launchSurveyIntegrationsPage(surveyName);
	
	}
	
	@Parameters({"IntegrationType"})
	@Test(description="3. Add Integrations")
	void TC03_AddIntegrationsToSurvey(@Optional("Slack") String integrationType) {
		integrtnPage.addNewIntegration(integrationType);
	}
	
	@Parameters({"Account","Medium","MediumValue"})
	@Test(description="4. Configure Integrations")
	void TC04_ConfigureIntegration(@Optional("Neurosensum") String integrationType,@Optional("Channel") String medium,@Optional("random") String mediumValue) {

		integrtnPage.configureSlackIntegration(integrationType,medium,mediumValue);
		integrtnPage.turnOnIntegration();
	}
	
	@Parameters({"SurveyName","IntegrationName"})
	@Test(description="5. Delete Integrations",enabled=false)
	void TC05_DeleteIntegrationsFromSurvey(@Optional("Star Survey Smoke Test")String surveyName,@Optional("New Slack Notification-1") String integrationName) {
		integrtnPage.goToHomePage();
		integrtnPage.launchSurveyIntegrationsPage(surveyName);
		integrtnPage.deleteIntegration(integrationName);
	}
	

	@Test(description = "6. Logout from survey sensum portal")
	void TC06_LogOutFromPortal() {
		integrtnPage.goToHomePage();
		loginPage.logoutFromPortal();
	}
}
