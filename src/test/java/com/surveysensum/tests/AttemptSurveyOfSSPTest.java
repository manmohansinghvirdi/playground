package com.surveysensum.tests;


import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.surveysensum.framework.PreAndPostTestEvents;
import com.surveysensum.helperUtils.PropFileHandler;
import com.surveysensum.keywords.SurveyRunnerPageFunctions;

public class AttemptSurveyOfSSPTest extends PreAndPostTestEvents {

	SurveyRunnerPageFunctions surveyRunner;
	PropFileHandler propFile;

	public void init() {
		surveyRunner = new SurveyRunnerPageFunctions(driver);
		propFile = PropFileHandler.getInstance("SmokeTestData");
	}

	@Parameters({"LinkLabel"})
	@Test(description = "1. Launch survey via anonymous link")
	void TC01_LaunchSurveyLink(@Optional("StarSurveySmokeTestLink") String link) {
		driver.launchApplication(propFile.readProperty(link));
	}
	
	@Test(description="2. Attempt Survey - give rating")
	void TC02_AttemptSurvey() {
		surveyRunner.attemptStarSurveyFirstQuestion(2);
	}

	@Test(description="3. Attempt Survey - give reason")
	void TC03_AttemptSurvey() {
		surveyRunner.attemptStarSurveySecondQuestion("Test Rating - 2");
	}
}
