package com.surveysensum.tests;

import static com.surveysensum.helperUtils.ConfigPropertyReader.getProperty;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.surveysensum.framework.PreAndPostTestEvents;
import com.surveysensum.keywords.SlackAppPageFunctions;

public class SlackIntegrationMessageTest extends PreAndPostTestEvents {

	SlackAppPageFunctions slackPage;
	public void init() {
		slackPage = new SlackAppPageFunctions(driver);
	}
	
	@Test(description = "1. Login to Slack")
	void TC01_LoginToSlackPortal() {
		slackPage.loginToSlackWebPortal(getProperty("slackWorkspace"), getProperty("slackUser"), getProperty("slackPassword"));
	}
	
	
	@Parameters({"surveyName","medium","mediumValue"})
	@Test(description="2. Verify Integration message")
	void TC02_verify(@Optional("Star Survey Smoke Test")String surveyName,@Optional("Channel")String medium,@Optional("random")String mediumValue) {
	slackPage.navigateToMedium(medium, mediumValue);
	slackPage.verifyIntegrationMessage(surveyName);
	}
	
	@Test(description="3. Sign out from Slack App")
	void TC03_signOutFromSlackWebApp() {
		slackPage.logOutFromSlackWebPortal();
	}
}
