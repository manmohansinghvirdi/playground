package com.surveysensum.tests;

import static com.surveysensum.helperUtils.ConfigPropertyReader.getProperty;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.surveysensum.framework.PreAndPostTestEvents;
import com.surveysensum.keywords.BuildPageFunctions;
import com.surveysensum.keywords.HomePageFunctions;
import com.surveysensum.keywords.LoginPageFunctions;

public class CreateSurveyOnSSPTest extends PreAndPostTestEvents {
	LoginPageFunctions loginPage;
	HomePageFunctions homePage;
	BuildPageFunctions buildPage;
	public void init() {
		 loginPage = new LoginPageFunctions(driver);
		 homePage = new HomePageFunctions(driver);
		 buildPage = new BuildPageFunctions(driver);
	}
	@Test(description = "1. Login to survey sensum portal")
	void TC01_LoginToPortal() {
		loginPage.loginToSurveySensumPortal(getProperty("UATURL"), getProperty("UATusername"), getProperty("UATpassword"));
		}
	
	@Parameters({"SurveyType","SurveyName"})
	@Test(description = "2. Create a survey")
	void TC02_CreateASurvey(@Optional("Star Rating Survey") String type,@Optional("Star Survey Smoke Test") String name) {
		homePage.createSurvey(type,name);
		
	}
	
	@Parameters({"SurveyName"})
	@Test(description="3. Edit questions on build page")
	void TC03_EditQuestions(@Optional("Star Survey Smoke Test") String name) {
		buildPage.launchSurveyBuildPage(name);
		buildPage.hitSetup();
		buildPage.editQuestion("How would you rate your recent experience with Smoke Test?");
		
	}
	
	@Test(description = "4. Logout from survey sensum portal")
	void TC04_LogOutFromPortal() {
		buildPage.goToHomePage();
		loginPage.logoutFromPortal();
	}
}
