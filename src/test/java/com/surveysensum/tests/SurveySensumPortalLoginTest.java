package com.surveysensum.tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.surveysensum.framework.PreAndPostTestEvents;
import com.surveysensum.keywords.LoginPageFunctions;

import static com.surveysensum.helperUtils.ConfigPropertyReader.getProperty;

public class SurveySensumPortalLoginTest extends PreAndPostTestEvents {
	LoginPageFunctions loginPage;
	public void init() {
		 loginPage = new LoginPageFunctions(driver);
	}

	@Test(description = "1. Login to survey sensum portal")
	void TC01_LoginToPortal() {
		loginPage.loginToSurveySensumPortal(getProperty("UATURL"), getProperty("UATusername"), getProperty("UATpassword"));
	}

	@Test(description = "2. Logout from survey sensum portal")
	void TC02_LogOutFromPortal() {
		loginPage.logoutFromPortal();
	}
}
