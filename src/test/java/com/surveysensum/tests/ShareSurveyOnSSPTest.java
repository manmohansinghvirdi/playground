package com.surveysensum.tests;

import static com.surveysensum.helperUtils.ConfigPropertyReader.getProperty;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.surveysensum.framework.PreAndPostTestEvents;
import com.surveysensum.helperUtils.PropFileHandler;
import com.surveysensum.keywords.HomePageFunctions;
import com.surveysensum.keywords.LoginPageFunctions;
import com.surveysensum.keywords.SharePageFunctions;

public class ShareSurveyOnSSPTest extends PreAndPostTestEvents {
	
	LoginPageFunctions loginPage;
	HomePageFunctions homePage;
	SharePageFunctions sharePage;
	PropFileHandler propFile;
	public void init() {
		 loginPage = new LoginPageFunctions(driver);
		 homePage = new HomePageFunctions(driver);
		 sharePage = new SharePageFunctions(driver);
		 propFile =  PropFileHandler.getInstance("SmokeTestData");
	}
	@Test(description = "1. Login to survey sensum portal")
	void TC01_LoginToPortal() {
		loginPage.loginToSurveySensumPortal(getProperty("UATURL"), getProperty("UATusername"), getProperty("UATpassword"));
		}
	
	@Parameters({"SurveyName","LinkLabel"})
	@Test(description="2. Share Page - Get Survey Link")
	void TC02_goToSharePage(@Optional("Star Survey Smoke Test")String name,@Optional("StarSurveySmokeTestLink")String link) {
		sharePage.launchSurveySharePage(name);
		propFile.writeToFile(link, sharePage.ReturnSurveyLink());
		
	}
	
	
	@Test(description = "3. Logout from survey sensum portal")
	void TC03_LogOutFromPortal() {
		sharePage.goToHomePage();
		loginPage.logoutFromPortal();
	}
}
