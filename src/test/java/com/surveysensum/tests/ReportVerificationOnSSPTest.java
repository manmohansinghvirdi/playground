package com.surveysensum.tests;

import static com.surveysensum.helperUtils.ConfigPropertyReader.getProperty;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.surveysensum.framework.PreAndPostTestEvents;
import com.surveysensum.keywords.LoginPageFunctions;
import com.surveysensum.keywords.ReportsPageFunctions;

public class ReportVerificationOnSSPTest extends PreAndPostTestEvents{
	LoginPageFunctions loginPage;
	ReportsPageFunctions reportsPage;
	public void init() {
		 loginPage = new LoginPageFunctions(driver);
		 reportsPage = new ReportsPageFunctions(driver);
	}

	
	@Test(description = "1. Login to survey sensum portal")
	void TC01_LoginToPortal() {
		loginPage.loginToSurveySensumPortal(getProperty("UATURL"), getProperty("UATusername"), getProperty("UATpassword"));
		}
	
	@Parameters({"SurveyName"})
	@Test(description = "2. verify total response and average rating")
	void TC02_GotoReportsAndVerifyResults(@Optional("Star Survey Smoke Test")String name) {
		reportsPage.goToReportsPage("Star Survey Smoke Test");
		reportsPage.verifyTotalNumberOfResponses(1);
//		reportsPage.verifyAverageRatingFigure(2);
	}

	@Test(description = "3. Logout from survey sensum portal")
	void TC03_LogOutFromPortal() {
		reportsPage.goToHomePage();
		loginPage.logoutFromPortal();
	}

}
