package com.surveysensum.keywords;


import org.openqa.selenium.By;

import com.surveysensum.framework.webDriverCreator;
import com.surveysensum.pageobjects.LoginPageObjects;

public class LoginPageFunctions {

	webDriverCreator driver;
	LoginPageObjects loginPO;
	public LoginPageFunctions(webDriverCreator driver) {
		this.driver = driver;
		loginPO = new LoginPageObjects();
	}
	
	public void loginToSurveySensumPortal(String URL,String username, String password) {
		driver.launchApplication(URL);
		driver.page.isElementDisplayed(loginPO.username_inputField);
		driver.page.enterText(loginPO.username_inputField, username);
		driver.page.isElementDisplayed(loginPO.password_inputField);
		driver.page.enterText(loginPO.password_inputField, password);
		driver.page.clickElement(loginPO.submit_Btn);
	}
	
	public void logoutFromPortal() {
		driver.page.isElementDisplayed(loginPO.profilePic);
		driver.page.clickElement(loginPO.profilePic);
		driver.page.isElementDisplayed(By.cssSelector("li[class='logout-btn']"));
		driver.page.clickElement(By.cssSelector("li[class='logout-btn']"));
		driver.page.isElementDisplayed(loginPO.submit_Btn);
	}

	
}
