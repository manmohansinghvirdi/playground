package com.surveysensum.keywords;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.surveysensum.framework.webDriverCreator;
import com.surveysensum.pageobjects.BuildPageObjects;

public class BuildPageFunctions extends CommonPageFunctions {
	webDriverCreator driver;
	BuildPageObjects buildPO;
	public BuildPageFunctions(webDriverCreator driver) {
		super(driver);
		this.driver = driver; 
		buildPO = new BuildPageObjects();
	}
	public void launchSurveyBuildPage(String surveyTitle) {
		driver.page.isElementDisplayed(By.xpath("//span[@title='"+surveyTitle+"']"));
		driver.page.hover(By.xpath("//span[@title='"+surveyTitle+"']/ancestor::div[@class='card']"));
		driver.page.clickElement(By.xpath("//span[@title='"+surveyTitle+"']/../../..//div[@data-tooltip='Build']"));
		
	}
	public void hitSetup() {
		driver.page.isElementDisplayed(buildPO.setup_Btn);
		driver.page.clickElement(buildPO.setup_Btn);
	}
	
	public void editQuestion(String question) {
		driver.page.isElementDisplayed(buildPO.defaultQuestion);
		driver.page.switchToFrame(driver.page.finder(buildPO.livePreview_iframe));
		driver.page.isElementDisplayed(buildPO.runnerText);
		driver.page.switchToDefaultContent();
		driver.page.clearKeysUsingActionBuilder(buildPO.defaultQuestion);
		driver.page.sendKeysByActionBuilder(buildPO.defaultQuestion, question);
		driver.page.switchToFrame(driver.page.finder(buildPO.livePreview_iframe));
		driver.page.wait.waitForElementToContainsText(driver.page.finder(buildPO.runnerText), question);
		driver.page.switchToDefaultContent();
		driver.page.clickElement(buildPO.followUpQuestion);
		driver.page.isElementDisplayed(buildPO.defaultFollowUp_ToggleSwitch);
		if(!driver.page.finder(buildPO.defaultFollowUpQuestion_label).getAttribute("class").contains("unClicked-color")){
			driver.page.clickElement(buildPO.defaultFollowUp_ToggleSwitch);
			
		}
		driver.page.isElementDisplayed(buildPO.editFollowUpQuestion_textArea);
	}
	
	

}
