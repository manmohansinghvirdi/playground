package com.surveysensum.keywords;

import com.surveysensum.framework.webDriverCreator;
import com.surveysensum.pageobjects.CommonPageObjects;

public class CommonPageFunctions {

	webDriverCreator driver;
	CommonPageObjects CommonPO ;
	CommonPageFunctions(webDriverCreator driver) {
		this.driver = driver;
		CommonPO = new CommonPageObjects();

	}
	
	public void goToHomePage() {
		driver.page.switchToDefaultContent();
		driver.page.isElementDisplayed(CommonPO.home_hyperlink);
		driver.page.clickElement(CommonPO.home_hyperlink);
	}

}
