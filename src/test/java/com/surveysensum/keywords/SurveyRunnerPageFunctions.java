package com.surveysensum.keywords;

import org.openqa.selenium.By;

import com.surveysensum.framework.webDriverCreator;
import com.surveysensum.pageobjects.SurveyRunnerPageObjects;

public class SurveyRunnerPageFunctions extends CommonPageFunctions {
	webDriverCreator driver;
	SurveyRunnerPageObjects surveyRunnerPO;
	public SurveyRunnerPageFunctions(webDriverCreator driver) {
		super(driver);
		this.driver = driver;
		surveyRunnerPO = new SurveyRunnerPageObjects();
	}
	public void attemptStarSurveyFirstQuestion(int rating) {
		driver.page.isElementDisplayed(surveyRunnerPO.starTray);
		driver.page.clickElement(By.xpath("//div[@class='rating-items']/div//div[@class='rating-lable' and text()='"+rating+"']"));
	}
	public void attemptStarSurveySecondQuestion(String answer) {
		driver.page.isElementDisplayed(surveyRunnerPO.runnerLongTextQuestion_InputField);
		driver.page.sendKeysByActionBuilder(surveyRunnerPO.runnerLongTextQuestion_InputField, answer);
		driver.page.clickElement(surveyRunnerPO.submit_Btn);
		driver.page.isElementDisplayed(surveyRunnerPO.brandLogo_Img);
	}

	
}
