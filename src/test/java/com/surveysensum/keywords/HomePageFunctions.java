package com.surveysensum.keywords;

import org.openqa.selenium.By;

import com.surveysensum.framework.webDriverCreator;
import com.surveysensum.pageobjects.HomePageObjects;

public class HomePageFunctions extends CommonPageFunctions {

	webDriverCreator driver;
	HomePageObjects HomePO;
	public HomePageFunctions(webDriverCreator driver) {
		super(driver);
		this.driver = driver;
		 HomePO = new HomePageObjects();
	}
	public void inviteUsers() {
		driver.page.isElementDisplayed(HomePO.inviteUser_Btn);
		driver.page.clickElement(HomePO.inviteUser_Btn);
		driver.page.isElementDisplayed(HomePO.close_Btn);
		driver.page.performClickByActionBuilder(HomePO.close_Btn);
	}
	
	public void createSurvey(String surveyType, String surveyTitle) {
		driver.page.isElementDisplayed(HomePO.createSurvey_Btn);
		driver.page.performClickByActionBuilder(HomePO.createSurvey_Btn);
		driver.page.isElementDisplayed(By.xpath("//div[normalize-space(text())='"+surveyType+"']"));
		driver.page.performClickByActionBuilder(By.xpath("//div[normalize-space(text())='"+surveyType+"']"));
		driver.page.isElementDisplayed(HomePO.surveyName_inputField);
		driver.page.enterText(HomePO.surveyName_inputField, surveyTitle);
		driver.page.clickElement(HomePO.create_Btn);
		driver.page.isElementDisplayed(HomePO.setup_Btn);
		goToHomePage();
//		driver.page.isElementDisplayed(HomePO.close_Btn);
//		driver.page.clickElement(HomePO.close_Btn);
	}
	
	
	
	public void archiveExistingSurvey(String surveyTitle) {
		driver.page.isElementDisplayed(By.xpath("//span[@title='"+surveyTitle+"']"));
 		driver.page.hover(By.xpath("//span[@title='"+surveyTitle+"']/ancestor::div[@class='card']"));
		driver.page.clickElement(By.xpath("//span[@title='"+surveyTitle+"']/../../../preceding-sibling::span/a"));
		driver.page.isElementDisplayed(By.xpath("//span[@title='"+surveyTitle+"']/../../../preceding-sibling::span//a[text()='Archive']"));
		driver.page.clickElement(By.xpath("//span[@title='"+surveyTitle+"']/../../../preceding-sibling::span//a[text()='Archive']"));
		driver.page.isElementDisplayed(HomePO.archive_Btn);
		driver.page.clickElement(HomePO.archive_Btn);
		driver.page.isElementDisplayed(HomePO.surveyArchived_Message);
	}
	
	public void navigateToArchives() {
		driver.page.isElementDisplayed(HomePO.archivesNavigation_Btn);
		driver.page.clickElement(HomePO.archivesNavigation_Btn);
	}
	
	public void navigateToAllSurveys() {
		driver.page.isElementDisplayed(HomePO.archivesNavigation_Btn);
		driver.page.clickElement(HomePO.archivesNavigation_Btn);
	
	}
	
	public void navigateToPeopleTab() {
		driver.page.isElementDisplayed(HomePO.peopleTab);
		driver.page.clickElement(HomePO.peopleTab);
	}
	
	public void navigateToSettingsTabs() {
		driver.page.isElementDisplayed(HomePO.settingsTab);
		driver.page.clickElement(HomePO.settingsTab);
	}
	
	
	public void deleteSurveyFromArchive(String surveyTitle) {
		driver.page.isElementDisplayed(By.xpath("//span[@title='"+surveyTitle+"']"));
		driver.page.hover(By.xpath("//span[@title='"+surveyTitle+"']/ancestor::div[@class='card']"));
		driver.page.clickElement(By.xpath("//span[@title='"+surveyTitle+"']/../../../preceding-sibling::span/a"));
		driver.page.isElementDisplayed(By.xpath("//span[@title='"+surveyTitle+"']/../../../preceding-sibling::span//a[text()='Delete']"));
		driver.page.clickElement(By.xpath("//span[@title='"+surveyTitle+"']/../../../preceding-sibling::span//a[text()='Delete']"));
		driver.page.isElementDisplayed(HomePO.delete_Btn);
		driver.page.clickElement(HomePO.delete_Btn);
		driver.page.isElementDisplayed(HomePO.surveyDeleted_Message);
	}
	public void addContactsInPeopleTab(String name, String email, String phoneNo) {
		driver.page.isElementDisplayed(HomePO.addContacts_Btn);
		driver.page.clickElement(HomePO.addContacts_Btn);
		driver.page.clickElement(HomePO.addManually_Option);
		driver.page.isElementDisplayed(HomePO.EnterName_InputField);
		driver.page.enterText(HomePO.EnterName_InputField, name);
		driver.page.enterText(HomePO.EnterEmail_InputField, email);
		driver.page.enterText(HomePO.EnterPhone_InputField, phoneNo);
		driver.page.clickElement(HomePO.saveAndExit_Btn);
		driver.page.isElementDisplayed(By.xpath("//table//tr[contains(.,'"+name+"') and contains(.,'"+email+"') and contains(.,'"+phoneNo+"')]"));
		driver.page.logSuccessMessage("Contact successfully added");
	}
	
	public void delete1Contact(String emailId) {
		driver.page.isElementDisplayed(By.xpath("//table//tr[contains(.,'"+emailId+"')]//*[name()='svg' and contains(.,'delete')]"));
		driver.page.clickElement(By.xpath("//table//tr[contains(.,'"+emailId+"')]//*[name()='svg' and contains(.,'delete')]"));
		driver.page.isElementDisplayed(By.xpath("//input[@placeholder='1']"));
		driver.page.enterText(By.xpath("//input[@placeholder='1']"),"1"); //Product check to confirm intent
		driver.page.clickElement(HomePO.delete_Btn);
		driver.page.isElementDisplayed(HomePO.contactDeleted_Message);
		
	}
	public void verifyUserSubscriptionPlan(String planName, String billingCycle) {
		driver.page.isElementDisplayed(HomePO.subscription_subTab);
		driver.page.clickElement(HomePO.subscription_subTab);
		driver.page.isElementDisplayed(HomePO.planName_Label);
		driver.page.verifyElementText(HomePO.planName_Label, planName);
		driver.page.verifyElementText(HomePO.billingCycle_Label, billingCycle);
		
	}
	
	public void searchUserInPeopleTab(String emailId) {
		driver.page.isElementDisplayed(HomePO.searchField);
		driver.page.enterText(HomePO.searchField, emailId);
		driver.page.clickElement(HomePO.search_Btn);
		
	}

	
	

}
