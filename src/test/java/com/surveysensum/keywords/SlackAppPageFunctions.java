package com.surveysensum.keywords;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.surveysensum.framework.webDriverCreator;
import com.surveysensum.pageobjects.SlackAppPageObjects;

public class SlackAppPageFunctions {
	webDriverCreator driver;
	SlackAppPageObjects slackAppPO;

	public SlackAppPageFunctions(webDriverCreator driver) {
		this.driver = driver;
		slackAppPO = new SlackAppPageObjects();
	}

	public void loginToSlackWebPortal(String workspaceURL, String userName, String password) {
		driver.launchApplication(workspaceURL);
		driver.page.isElementDisplayed(slackAppPO.username_inputField);
		driver.page.sendKeysByActionBuilder(slackAppPO.username_inputField, userName);
		driver.page.isElementDisplayed(slackAppPO.password_inputField);
		driver.page.sendKeysByActionBuilder(slackAppPO.password_inputField, password);
		driver.page.clickElement(slackAppPO.signIn_Btn);
		driver.page.executeJavascript("window.open()");
		driver.page.switchWindow();
		driver.launchApplication(workspaceURL);
		if(driver.page.isElementPresent(slackAppPO.continueInBrowser_Btn)) {
			driver.page.isElementDisplayed(slackAppPO.continueInBrowser_Btn);
			driver.page.performClickByActionBuilder(slackAppPO.continueInBrowser_Btn);
		}
		
	}

	public void navigateToMedium(String medium, String mediumValue) {
		if (medium.equalsIgnoreCase("Channel")) {
			driver.page.isElementDisplayed(By.xpath("//span[@class='p-channel_sidebar__name' and contains(.,'"+mediumValue+"')]"));
			driver.page.performClickByActionBuilder(By.xpath("//span[@class='p-channel_sidebar__name' and contains(.,'"+mediumValue+"')]"));
		}
	}
	
	public void verifyIntegrationMessage(String surveyName) {
		driver.page.isElementDisplayed(By.xpath("//div[@class='p-section_block__text' and contains(.,'"+surveyName+"')]"));
		driver.page.verifyElementTextContains(By.xpath("//div[@class='p-section_block__text' and contains(.,'"+surveyName+"')]"), "Hello, its Surveysensum. Whenever someone completes");
	}
	
	public void logOutFromSlackWebPortal() {
		driver.page.isElementDisplayed(slackAppPO.slackApp_avatarImg);
		driver.page.performClickByActionBuilder(slackAppPO.slackApp_avatarImg);
		driver.page.isElementDisplayed(slackAppPO.signOut_MenuBtn);
		driver.page.clickElement(slackAppPO.signOut_MenuBtn);
	}
}
