package com.surveysensum.keywords;

import org.openqa.selenium.By;

import com.surveysensum.framework.webDriverCreator;
import com.surveysensum.pageobjects.IntegrationsPageObjects;

public class IntegrationsPageFunctions extends CommonPageFunctions {

	webDriverCreator driver;
	IntegrationsPageObjects integrtnPO;
	public IntegrationsPageFunctions(webDriverCreator driver) {
		super(driver);
		this.driver = driver;
		integrtnPO = new IntegrationsPageObjects();
	}
	
	public void launchSurveyIntegrationsPage(String surveyTitle) {
		driver.page.isElementDisplayed(By.xpath("//span[@title='" + surveyTitle + "']"));
		driver.page.hover(By.xpath("//span[@title='" + surveyTitle + "']/ancestor::div[@class='card']"));
		driver.page
				.clickElement(By.xpath("//span[@title='" + surveyTitle + "']/../../..//div[@data-tooltip='Integrations']"));
	}

	public void addNewIntegration(String integrationType) {
		driver.page.isElementDisplayed(integrtnPO.addNewIntegration_Btn);
		driver.page.clickElement(integrtnPO.addNewIntegration_Btn);
		driver.page.isElementDisplayed(By.xpath("//div[@class='app-name' and contains(.,'"+integrationType+"')]"));
		driver.page.performClickByActionBuilder(By.xpath("//div[@class='app-name' and contains(.,'"+integrationType+"')]/ancestor::div[@class='app-card']//button[text()='Select']"));
	}

	public void configureSlackIntegration(String accountName, String medium, String mediumValue) {
		driver.page.isElementDisplayed(integrtnPO.selectSlackAccount_Dropdown);
		driver.page.performClickByActionBuilder(integrtnPO.selectSlackAccount_Dropdown);
		driver.page.isElementDisplayed(By.xpath("//li[@class='accounts-scroller']/ul[contains(.,'"+accountName+"')]"));
		driver.page.clickElement(By.xpath("//li[@class='accounts-scroller']/ul[contains(.,'"+accountName+"')]"));
		
		driver.page.isElementDisplayed(integrtnPO.selectChannelOrUser_Dropdown);
		driver.page.performClickByActionBuilder(integrtnPO.selectChannelOrUser_Dropdown);
		driver.page.isElementDisplayed(By.xpath("//ul[contains(@class,'select-channel-user-dropdown')]/li[contains(.,'"+medium+"')]"));
		driver.page.performClickByActionBuilder(By.xpath("//ul[contains(@class,'select-channel-user-dropdown')]/li[contains(.,'"+medium+"')]"));
	
		driver.page.waitForElementToDisplay(integrtnPO.loaderContainer_Dropdown);
		driver.page.performClickByActionBuilder(integrtnPO.loaderContainer_Dropdown);
		driver.page.isElementDisplayed(integrtnPO.search_inputField);
		driver.page.sendKeysByActionBuilder(integrtnPO.search_inputField, mediumValue);
		driver.page.isElementDisplayed(By.xpath("//li[@class='channels-users-scroller']/ul[contains(.,'"+mediumValue+"')]"));
		driver.page.performClickByActionBuilder(By.xpath("//li[@class='channels-users-scroller']/ul[contains(.,'"+mediumValue+"')]"));
		
		
	}
	
	public void deleteIntegration(String integrationName) {
		driver.page.isElementDisplayed(By.xpath("//a[contains(.,'"+integrationName+"')]/ancestor::tr//div[@class='dropdown']/a"));
		driver.page.performClickByActionBuilder(By.xpath("//a[contains(.,'"+integrationName+"')]/ancestor::tr//div[@class='dropdown']/a"));
		driver.page.isElementDisplayed(integrtnPO.delete_Btn);
		driver.page.performClickByActionBuilder(integrtnPO.delete_Btn);
		driver.page.isElementDisplayed(integrtnPO.confirm_Btn);
		driver.page.performClickByActionBuilder(integrtnPO.confirm_Btn);
		driver.page.isElementDisplayed(integrtnPO.deleteSuccessful_Msg);
	}
	
	public void turnOnIntegration() {
		driver.page.isElementDisplayed(integrtnPO.turnOn_Btn);
		driver.page.performClickByActionBuilder(integrtnPO.turnOn_Btn);
		driver.page.isElementDisplayed(integrtnPO.addSuccessful_Msg);
	}
	

}
