package com.surveysensum.keywords;

import org.openqa.selenium.By;

import com.surveysensum.framework.webDriverCreator;
import com.surveysensum.pageobjects.ReportsPageObjects;

public class ReportsPageFunctions extends CommonPageFunctions {

	webDriverCreator driver;
	ReportsPageObjects reportsPO;
	public ReportsPageFunctions(webDriverCreator driver) {
		super(driver);
		this.driver = driver;
		reportsPO = new ReportsPageObjects();
	}
	public void goToReportsPage(String surveyTitle) {
		driver.page.isElementDisplayed(By.xpath("//span[@title='" + surveyTitle + "']"));
		driver.page.hover(By.xpath("//span[@title='" + surveyTitle + "']/ancestor::div[@class='card']"));
		driver.page
				.clickElement(By.xpath("//span[@title='" + surveyTitle + "']/../../..//div[@data-tooltip='Reports']"));	
	}
	
	public void verifyTotalNumberOfResponses(int totalResponses) {
		driver.page.isElementDisplayed(reportsPO.totalResponse_text);
		driver.page.wait.waitForElementToContainsText(driver.page.finder(reportsPO.totalResponse_text), Integer.toString(totalResponses));
		driver.page.verifyElementText(reportsPO.totalResponse_text, Integer.toString(totalResponses));
		
	}
	 
	public void verifyAverageRatingFigure(int avgResponse) {
		driver.page.isElementDisplayed(reportsPO.averageRating_text);
		driver.page.verifyElementTextContains(reportsPO.averageRating_text, Integer.toString(avgResponse));
	}
	

}
