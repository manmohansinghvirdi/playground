package com.surveysensum.keywords;

import org.openqa.selenium.By;

import com.surveysensum.framework.webDriverCreator;
import com.surveysensum.pageobjects.SharePageObjects;

public class SharePageFunctions extends CommonPageFunctions {
	webDriverCreator driver;
	SharePageObjects sharePO;

	public SharePageFunctions(webDriverCreator driver) {
		super(driver);
		this.driver = driver;
		sharePO = new SharePageObjects();
	}

	public void launchSurveySharePage(String surveyTitle) {
		driver.page.isElementDisplayed(By.xpath("//span[@title='" + surveyTitle + "']"));
		driver.page.hover(By.xpath("//span[@title='" + surveyTitle + "']/ancestor::div[@class='card']"));
		driver.page
				.clickElement(By.xpath("//span[@title='" + surveyTitle + "']/../../..//div[@data-tooltip='Sharing']"));
	}

	public String ReturnSurveyLink() {
		driver.page.isElementDisplayed(sharePO.copyLink_Btn);
		driver.page.clickElement(sharePO.copyLink_Btn);
		driver.page.isElementDisplayed(sharePO.anonymousShare_link);
		String surveyLink = driver.page.finder(sharePO.anonymousShare_link).getAttribute("value");
		driver.page.logSuccessMessage("Anonymous link retrieved: "+ surveyLink);
//		String clipboardContent = driver.page.getCBContents();
//		System.out.println(clipboardContent); //works on windows, permissions issue on Linux

		return surveyLink;
	}

}
