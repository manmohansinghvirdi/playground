package com.surveysensum.pageobjects;

import org.openqa.selenium.By;

public class SurveyRunnerPageObjects {
	public By starTray = By.xpath("//div[@class='rating-items']");
	public By submit_Btn = By.xpath("//button[text()='Submit']");
	public By runnerLongTextQuestion_InputField = By.xpath("//runner-long-text-question//textarea");
	public By brandLogo_Img = By.xpath("//img[@alt='Brand Logo']");

}
