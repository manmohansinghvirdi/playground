package com.surveysensum.pageobjects;

import org.openqa.selenium.By;

public class SharePageObjects {

	public By copyLink_Btn = By.xpath("//input[@value='Copy Link']");
	public By anonymousShare_link = By.cssSelector(".form-input.disable-no-opacity.ng-untouched.ng-pristine.ng-valid");
	
}
