package com.surveysensum.pageobjects;

import org.openqa.selenium.By;

public class IntegrationsPageObjects {

	public By addNewIntegration_Btn = By.xpath("//button[text()='Add New Integration']");
	public By selectSlackAccount_Dropdown = By.xpath("//div[@class='select-slack-account']//a");
	public By selectChannelOrUser_Dropdown = By.xpath("//a[@id='selectChannelOrUser']");
	public By loaderContainer_Dropdown = By.xpath("//div[contains(@class,'loader-dropdown-container')]//a");
	public By search_inputField = By.xpath("//input[@placeholder='Search…']");
	public By turnOn_Btn = By.xpath("//span[normalize-space(text())='Turn On']");
	public By addSuccessful_Msg = By.xpath("//span[text()='Integration turned on successfully']");
	public By deleteSuccessful_Msg = By.xpath("//span[text()='Deleted successfully']");
	public By delete_Btn = By.xpath("//a[contains(.,'Delete')]");
	public By confirm_Btn = By.xpath("//span[text()='Confirm']");
	
	
}
