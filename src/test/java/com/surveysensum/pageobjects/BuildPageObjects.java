package com.surveysensum.pageobjects;

import org.openqa.selenium.By;

public class BuildPageObjects {

	public By setup_Btn = By.xpath("//button[text()='Setup']");
	public By defaultQuestion= By.cssSelector("div[class='quest-text']");
	public By runnerText = By.xpath("//runner-title-text//h1");
	public By followUpQuestion = By.cssSelector("div[class='follow-up-heading']  > div[class$='arrow-rotate']");
	public By defaultFollowUpQuestion_label = By.xpath("//div[normalize-space(text())='Default (Same for all respondents)']");
	public By defaultFollowUp_ToggleSwitch = By.cssSelector("div[class='follow-up-box'] div[class='form-label'] i");
	public By editFollowUpQuestion_textArea = By.cssSelector("div[class='form-group']  textarea");
	public By close_Btn = By.xpath("//*[normalize-space(text())='Close']");
	public By livePreview_iframe = By.cssSelector("iframe[src$='live-preview']");
	
}
