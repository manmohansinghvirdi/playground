package com.surveysensum.pageobjects;

import org.openqa.selenium.By;

public class SlackAppPageObjects {

	public By username_inputField = By.id("email");
	public By password_inputField = By.id("password");
	public By signIn_Btn = By.xpath("//button[@data-qa='signin_button']");
	public By close_Btn = By.xpath("//button[@aria-label='Close']");
	public By continueInBrowser_Btn = By.xpath("//button[contains(.,'continue in browser')]");
	public By slackApp_avatarImg = By.xpath("//div[contains(@class,'p-ia__nav__user')]/span");
	public By signOut_MenuBtn = By.xpath("//div[contains(@class,'c-menu_item__label') and contains(.,'Sign out')]");
	
	
}
