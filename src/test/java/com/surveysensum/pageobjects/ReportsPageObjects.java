package com.surveysensum.pageobjects;

import org.openqa.selenium.By;

public class ReportsPageObjects {

	public By totalResponse_text = By.cssSelector(".total-response .ticket-score");
	public By averageRating_text = By.xpath("//app-score//div[contains(@style,'absolute') and contains(.,'Average Rating')]");

}
