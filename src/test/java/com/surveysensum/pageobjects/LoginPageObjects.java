package com.surveysensum.pageobjects;

import org.openqa.selenium.By;

public class LoginPageObjects {

	public By profilePic = By.xpath("//a//*[@class='profile-pic']");
	public By username_inputField = By.id("username");
	public By password_inputField = By.id("password");
	public By submit_Btn = By.id("submit");
	
}
