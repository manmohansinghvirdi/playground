package com.surveysensum.pageobjects;

import org.openqa.selenium.By;

public class HomePageObjects {

	//All Suveys tab
	
	public By inviteUser_Btn = By.cssSelector(".people-add>button>img");
	public By close_Btn = By.cssSelector("span[class='cross-btn'], img[src$='close.svg']");
	public By createSurvey_Btn = By.id("prod_createSurvey");
	public By archive_Btn = By.xpath("//button[contains(@class,'danger') and contains(.,'Archive')]");
	public By delete_Btn = By.xpath("//button[contains(@class,'danger') and contains(.,'Delete')]");
	public By create_Btn = By.xpath("//button/span[text()='Create']");
	public By setup_Btn = By.xpath("//button[text()='Setup']");
	public By surveyArchived_Message = By.xpath("//span[text()='Survey Archived']");
	public By surveyDeleted_Message = By.xpath("//span[text()='Survey deleted successfully']");
	public By archivesNavigation_Btn = By.id("archived_survey_nav_icon");
	public By allSurveysNavigation_Btn = By.id("all_survey_home_nav_icon");
	public By surveyName_inputField = By.xpath("//input[@placeholder='Enter a survey name']");
	
	
	//People Tab
	public By peopleTab = By.xpath("//li[normalize-space(text())='People']");
	public By addContacts_Btn = By.xpath("//a[normalize-space(text())='Add Contacts']");
	public By addManually_Option = By.partialLinkText("Add Manually");
	public By contactDeleted_Message = By.xpath("//span[text()='1 contact deleted']");
	public By EnterName_InputField = By.xpath("//input[@placeholder='Enter Name']");
	public By EnterEmail_InputField = By.xpath("//input[@placeholder='Enter Email']");
	public By EnterPhone_InputField = By.xpath("//input[@placeholder='Enter Phone Number']");
	public By saveAndExit_Btn = By.xpath("//button/span[text()='Save & Exit']");
	public By searchField = By.cssSelector("input[placeholder^='Search']");
	public By search_Btn = By.cssSelector("img[src*='search']");
	
	//Settings Tab
	public By settingsTab = By.xpath("//li[normalize-space(text())='Settings']");
	public By subscription_subTab = By.xpath("//span[normalize-space(text())='Subscription']");
	public By planName_Label = By.cssSelector(".plan-name > span:first-child");
	public By billingCycle_Label = By.className("plan-sub-txt");
	
	
	

}
