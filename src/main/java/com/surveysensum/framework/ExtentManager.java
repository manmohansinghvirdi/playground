package com.surveysensum.framework;

import java.io.File;

import org.openqa.selenium.Platform;
import org.openqa.selenium.net.LinuxEphemeralPortRangeDetector;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

/*
 * https://www.swtestacademy.com/extent-reports-version-3-reporting-testng/
 * */
public class ExtentManager {

	private static ExtentReports extent;
	private static Platform platform;
	private static String reportFileName = "Extent-Test-Report.html";
	private static String macPath = System.getProperty("user.dir") + "/TestReport";
	private static String windowsPath = System.getProperty("user.dir") + "\\TestReport";
	private static String linuxPath = System.getProperty("user.dir")+File.separator+"TestReport";
	private static String macReportFileLoc = macPath + "/" + reportFileName;
	private static String winReportFileLoc = windowsPath + "\\" + reportFileName;
	private static String linuxReportFileLoc = linuxPath + File.separator+reportFileName; // TODO: Use this as a generic initialisation for all paths

	public static ExtentReports getInstance() {
		if (extent == null) {
			createInstance();
		}
		return extent;
	}

	public static ExtentReports createInstance() {
		String fileName = getReportFileLocation(getCurrentPlatform());
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
		htmlReporter.config().setTheme(Theme.DARK);
		htmlReporter.config().setDocumentTitle(fileName);
		htmlReporter.config().setEncoding("utf-8");
		htmlReporter.config().setReportName(fileName);
		htmlReporter.config().setCSS(".r-img { width : 30%; }");
		htmlReporter.config().enableTimeline(true);
		
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		
		return extent;
	}

	/**************************************************************************
	 * Custom code for determining file path location based on OS/environment
	 **************************************************************************/

	private static String getReportFileLocation(Platform platform) {
		String reportFileLocation = null;
		switch (platform) {
		case MAC:
			reportFileLocation = macReportFileLoc;
			createReportPath(macPath);
			System.out.println("ExtentReport Path for MAC: " + macPath + "\n");
			break;
		case WINDOWS:
			reportFileLocation = winReportFileLoc;
			createReportPath(windowsPath);
			System.out.println("ExtentReport Path for WINDOWS: " + windowsPath + "\n");
			break;
		case LINUX:
			reportFileLocation = linuxReportFileLoc;
			createReportPath(linuxPath);
			System.out.println("ExtentReport Path for Linux:"+linuxPath+" \n");
			break;
		default:
			System.out.println("ExtentReport path has not been set! There is a problem!\n");
			break;	
		}
		return reportFileLocation;
	}

	// Get current platform
	private static Platform getCurrentPlatform() {
		if (platform == null) {
			String operSys = System.getProperty("os.name").toLowerCase();
			if (operSys.contains("win")) {
				platform = Platform.WINDOWS;
			} else if (operSys.contains("nix") || operSys.contains("nux") || operSys.contains("aix")) {
				platform = Platform.LINUX;
			} else if (operSys.contains("mac")) {
				platform = Platform.MAC;
			}
		}
		return platform;
	}
	
	//Create the report path if it does not exist
    private static void createReportPath (String path) {
        File testDirectory = new File(path);
        if (!testDirectory.exists()) {
            if (testDirectory.mkdir()) {
                System.out.println("Directory: " + path + " is created!" );
            } else {
                System.out.println("Failed to create directory: " + path);
            }
        } else {
            System.out.println("Directory already exists: " + path);
        }
    }

}